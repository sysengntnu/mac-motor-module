from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

setup(
	name='mac_motor_module',
	version='0.1.0.dev',
	author='Chriss Grimholt',
    author_email='grimholt@me',
	description='Serial interface for the Mac050',
	url='https://bitbucket.org/sysengntnu/mac-motor-module',
    packages=['mac_motor_module'],
	)
